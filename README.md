# Bodované úlohy na druhé pololetí 2019/2020

Zadání je v jednotlivých `.java` souborech ve složce `src/`.

Pro odevzdání vytvořte, prosím, vlastní *fork* a přidejte
uživatele `vhotspur` jako člena (nezapomeňte nastavit projekt
jako *Private*).

Součástí repozitáře je i soubor `.project`, který lze využít pro
import do Eclipse IDE.
